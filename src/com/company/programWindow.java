package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class programWindow extends JFrame implements ActionListener
{
	private JTextField textField = new JTextField(15);
	JButton button = new JButton("Check Number");
	private int num = (int) (Math.random() * 101);
	private int counter = 0;
	
	public programWindow()
	{
		JOptionPane.showMessageDialog(null, "Proposed a number from 0 to 100, you have 7 tries to guess it");
		
		setLayout(new FlowLayout());
		add(textField);
		add(button);
		
		button.addActionListener(this);
		textField.addActionListener(this);
		
		
		setResizable(false);
		setTitle("Guess The Number");
		setBounds(new Rectangle(400, 250, 300, 150));
		setVisible(true);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == button || e.getSource() == textField)
		{
			int x = Integer.parseInt(textField.getText());
			if (x > 100 || x < 0)
			{
				JOptionPane.showMessageDialog(null, "The number you entered is out of range");
			}
			else if (x == num)
			{
				JOptionPane.showMessageDialog(null, "You guessed the number");
				System.exit(1);
			}
			else if (x > num)
			{
				JOptionPane.showMessageDialog(null, "Your number is bigger than proposed one");
				counter++;
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Your number is less than proposed one");
				counter++;
			}
			if (counter == 7)
			{
				JOptionPane.showMessageDialog(null, "GAME OVER");
				System.exit(1);
			}
		}
	}
	
}

